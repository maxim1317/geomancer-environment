#! /bin/bash

#################################################
#############        STEP #0        #############
############# SETING SOME CONSTANTS #############
#################################################
DOCKER_NETWORK_NAME=geonet
DOCKER_SUBNET=10.17.0.0/24

PG_IP=10.17.0.2
PG_PORT=5432
PG_ADDRESS=$PG_IP:$PG_PORT

PG_DBNAME=gis
PG_UNAME=docker
PG_PASS=docker

OSM_DATA_CENTRAL_RUSSIA_DIR=https://download.geofabrik.de/russia/
OSM_DATA_CENTRAL_RUSSIA_FILE=central-fed-district-latest.osm.pbf
OSM_DATA_CENTRAL_RUSSIA_ADDR=$OSM_DATA_CENTRAL_RUSSIA_DIR$OSM_DATA_CENTRAL_RUSSIA_FILE

PSQL_ADD_COLUMN=sql/add_wkt_columns.sql
PSQL_CONVERT=sql/convert_geometry_to_wkt.sql

OSM2PGSQL_DEPS="make cmake g++ libboost-dev libboost-system-dev \
  libboost-filesystem-dev libexpat1-dev zlib1g-dev \
  libbz2-dev libpq-dev libproj-dev lua5.2 liblua5.2-dev"
OSM2PGSQL=osm2pgsql

PSQL=psql
#################################################



#################################################
#######              STEP #1              #######
####### STARTING POSTGIS DOCKER CONTAINER #######
#################################################
echo "Starting PostGIS"
docker network create \
    --subnet $DOCKER_SUBNET \
    $DOCKER_NETWORK_NAME \
    2>/dev/null \
&& \
    echo "Created docker network ${DOCKER_NETWORK_NAME}: ${DOCKER_SUBNET}. To remove execute\ndocker network rm ${DOCKER_NETWORK_NAME}" \
|| \
    true

(
    cd docker-postgis/ \
    && 
        docker-compose up -d \
    && \
        echo "PostGIS has started on ${PG_ADDRESS}") \
    && \
        echo "PostGIS has started" \
    || \
        true
#################################################



#################################################
###############      STEP #2      ###############
############### DOWNLOAD OSM DATA ###############
#################################################
echo "Loading OSM data"
if [ -f "$OSM_DATA_CENTRAL_RUSSIA_FILE" ]; then
    echo "$OSM_DATA_CENTRAL_RUSSIA_FILE already exists"
else
    wget $OSM_DATA_CENTRAL_RUSSIA_ADDR -q --show-progress --progress=bar:force:noscroll
fi
#################################################



#################################################
#########            STEP #3            #########
######### IMPORTING OSM DATA TO POSTGIS #########
#################################################
echo "Importing OSM data to PostGIS (takes ~6 hours)"

OSM2PGSQL_INSTALLED=$(dpkg-query -W --showformat='${Status}\n' ${OSM2PGSQL}|grep "install ok installed")
echo Checking for $OSM2PGSQL: $OSM2PGSQL_INSTALLED
if [ "" == "$OSM2PGSQL_INSTALLED" ]; then
  echo "No ${OSM2PGSQL}. Installing."
  sudo apt-get --force-yes --yes install $OSM2PGSQL_DEPS
  sudo apt-get --force-yes --yes install $OSM2PGSQL
fi

(export PGPASSWORD=$PG_PASS && $OSM2PGSQL --slim -H $PG_IP -P $PG_PORT -d $PG_DBNAME -U $PG_UNAME $OSM_DATA_CENTRAL_RUSSIA_FILE)
#################################################



#################################################
#####                STEP #4                #####
##### CONVERTING GEOMETRY TO WKT IN POSTGIS #####
#################################################
echo "Converting geometry data to WKT"

PSQL_INSTALLED=$(dpkg-query -W --showformat='${Status}\n' ${PSQL}|grep "install ok installed")
echo Checking for $PSQL: $PSQL_INSTALLED
if [ "" == "$PSQL_INSTALLED" ]; then
  echo "No ${PSQL}. Installing."
  sudo apt-get --force-yes --yes install $PSQL
fi

(export PGPASSWORD=$PG_PASS && $PSQL -h $PG_IP -p $PG_PORT -d $PG_DBNAME -U $PG_UNAME -w -f $PSQL_ADD_COLUMN)
(export PGPASSWORD=$PG_PASS && $PSQL -h $PG_IP -p $PG_PORT -d $PG_DBNAME -U $PG_UNAME -w -f $PSQL_CONVERT)
#################################################
