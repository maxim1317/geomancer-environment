UPDATE 
	planet_osm_point
SET 
	WKT = ST_AsText(way)
WHERE
	WKT IS NOT NULL;

UPDATE 
	planet_osm_line
SET 
	WKT = ST_AsText(way)
WHERE
	WKT IS NOT NULL;

UPDATE 
	planet_osm_polygon
SET 
	WKT = ST_AsText(way)
WHERE
	WKT IS NOT NULL;

UPDATE 
	planet_osm_roads
SET 
	WKT = ST_AsText(way)
WHERE
	WKT IS NOT NULL;