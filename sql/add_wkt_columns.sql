ALTER TABLE 
    planet_osm_point 
ADD 
    WKT TEXT;

ALTER TABLE 
    planet_osm_line 
ADD 
    WKT TEXT;

ALTER TABLE 
    planet_osm_polygon 
ADD 
    WKT TEXT;

ALTER TABLE 
    planet_osm_roads 
ADD 
    WKT TEXT;